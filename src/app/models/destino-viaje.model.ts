export class DestinoViaje {
	private selected: boolean;
	public servicios: string[];
	public id: number;

	constructor(public nombre:string,public url:string){ 
		this.servicios = ['pileta', 'desayuno'];
		this.id = 1;
	}

	isSelected(): boolean{
		return this.selected;
	}

	setSelected(s:boolean){
		this.selected = s;
	}
};