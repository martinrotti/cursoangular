import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  searchResults: string[];

  constructor(fb: FormBuilder) { 
  	this.onItemAdded = new EventEmitter();
  	this.fg = fb.group({
  		nombre: ['', Validators.compose([
  			Validators.required,
  			this.nombreValidator,
        this.hayInput
  		])],
  		url: ['']
  		});

  	this.fg.valueChanges.subscribe((form: any) => {	//cada vez que cambia el valor del form
  		//console.log('cambiooo');	
  		});
  }

  ngOnInit() {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input') //genera un observable de eventos de input 
      .pipe(  //operaciones secuenciales(en serie), la entrada a cada operacion es la salida de la anterior
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), //cuando teclean, agarro el valor del input
        filter(text => text.length > 2), //sigue a la siguiente operacion si la entrada es > 2, con la salida text
        debounceTime(200), //stop por 200ms 
        distinctUntilChanged(), //avanza a la siguiente operacion si lo que le llega es distinto a lo que le llego anteriormente (si cambio el input)
        switchMap(() => ajax('/assets/datos.json')) //el texto se lo pasamos al ajax, en este caso responde siempre con el arreglo definido en assets/datos.json
      ).subscribe(ajaxResponse => {
        this.searchResults = ajaxResponse.response;
        });
  }

  guardar(nom: string, url: string): boolean {
  	const d = new DestinoViaje(nom, url);
  	this.onItemAdded.emit(d);
  	return false;
  }

  nombreValidator(control: FormControl): {[s: string]: boolean} {
  	const l = control.value.toString().trim().length;
  	if (l > 0 && l < 5){
  		return { invalidNombre: true}	//json
  	}
  	return null;
  }

  hayInput(control: FormControl): {[s: string]: boolean} {
    const l = control.value.toString().trim().length;
    if (l > 0){
      return { inputMayorACero: true} //json
    }
    return null;
  }

}
