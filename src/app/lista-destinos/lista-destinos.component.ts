import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
	//destinos: DestinoViaje[];
  updates: string[];

  constructor(private destinosApiClient:DestinosApiClient) { 
  	//this.destinos = [];
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((d: DestinoViaje) =>{
      if (d != null){
        this.updates.push('se ha elegido a' + d.nombre);
      }
      });
  }

  ngOnInit() {
  }

  agregado(d: DestinoViaje){
  	//this.destinos.push(d);
  	//console.log(this.destinos);
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje){ //desmarcamos como elegidos a el resto y marcamos al elegido
    this.destinosApiClient.elegir(d);
  }

}
